//
//  MainCoordinatorTests.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import Quick
import Nimble
import UIKit

@testable import MVVM_C

class MainCoordinatorTests: QuickSpec {
    override func spec() {
        describe("MainCoordinator") {
            var mockNavigationController: MockNavigationController!
            var mockViewController: MockViewController!
            var coordinator: MainCoordinator!

            beforeEach {
                coordinator = MainCoordinator()
                mockViewController = MockViewController()
                
                mockNavigationController = MockNavigationController(rootViewController: mockViewController)
                mockNavigationController.viewControllers = [mockViewController]
            }
            
            describe("navigate") {
                context("when pushSecondScreen is selected") {
                    beforeEach {
                        coordinator.navigate(mockViewController, to: .pushSecondScreen)
                    }
                    
                    it("should try to present SecondViewController") {
                        expect(mockNavigationController.viewControllerPushed).to(beAnInstanceOf(SecondViewController.self))
                    }
                }
                
                context("when presentSecondScreen is selected") {
                    beforeEach {
                        coordinator.navigate(mockViewController, to: .presentSecondScreen)
                    }
                    
                    it("should try to present SecondViewController") {
                        expect(mockViewController.viewControllerPresented).to(beAnInstanceOf(SecondViewController.self))
                    }
                }
                
                context("when none is selected") {
                    beforeEach {
                        coordinator.navigate(mockViewController, to: .none)
                    }
                    
                    it("should not navigate") {
                        expect(mockViewController.viewControllerPresented).to(beNil())
                        expect(mockNavigationController.viewControllerPushed).to(beAnInstanceOf(MockViewController.self))
                    }
                }
            }
        }
    }
}
