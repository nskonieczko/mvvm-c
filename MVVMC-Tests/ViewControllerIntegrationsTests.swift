//
//  ViewControllerIntegrationsTests.swift
//  ViewControllerIntegrationsTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import Quick
import Nimble
import XCTest

@testable import MVVM_C

class ViewControllerIntegrationsTests: QuickSpec {
    override func spec() {
        describe("ViewController") {
            var viewModel: ViewControllerViewModel!
            var mockTableView: MockTableView!
            var mockAPIClient: MockAPIClient!
            
            var viewController: ViewController!
            
            beforeEach {
                mockTableView = MockTableView()
                mockAPIClient = MockAPIClient()
                viewModel = ViewControllerViewModel(apiClient: mockAPIClient)
                viewController = ViewController(viewModel: viewModel, coordinator: MainCoordinator())
                viewController.tableView = mockTableView
                
                _ = viewController.view
            }
            
            describe("viewDidAppear") {
                context("when called") {
                    beforeEach {
                        viewController.viewDidAppear(true)
                    }
                    
                    it("should call to get API Data once") {
                        expect(mockAPIClient.getAPIDataCallCount).to(equal(1))
                    }
                    
                    it("should reload tableView data, when delegate returns") {
                        mockAPIClient.getCustomerDataCompletions.last?(.init(firstName: .mock, lastName: .mock))
                        
                        expect(mockTableView.reloadDataCallCount).to(equal(1))
                    }
                }
            }
        }
    }
}
