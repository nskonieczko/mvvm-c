//
//  MockMainCoordinator.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

@testable import MVVM_C

class MockMainCoordinator: MainCoordinator {
    var routeSelected: MainCoordinator.Route?
    
    override init() { }
    
    override func navigate(_ context: UIViewController, to route: MainCoordinator.Route) {
        routeSelected = route
    }
}
