//
//  MockViewControllerViewModel.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation
import XCTest

@testable import MVVM_C

class MockViewControllerViewModel: ViewControllerViewModelProtocol {
    var delegate: ViewControllerViewModelDelegate?
    
    var numberOfRowsInSectionCalls: [Int] = []
    var numberOfRowsInSectionReturnValue: Int = .mock
    var numberOfRowsInSectionCallCount: Int {
        numberOfRowsInSectionCalls.count
    }
    
    var getAPIDataCallCount = 0
    var routeToReturn: MainCoordinator.Route?
    
    var cellNameToReturn: String?
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        numberOfRowsInSectionCalls.append(section)
        return numberOfRowsInSectionReturnValue
    }
    
    func getAPIData() {
        getAPIDataCallCount += 1
    }
    
    func route(for indexPath: IndexPath) -> MainCoordinator.Route {
        guard let route = routeToReturn else {
            XCTFail("You did not set `routeToReturn`")
            return .none
        }
        
        return route
    }
    
    func cellName(for indexPath: IndexPath) -> String {
        guard let cellNameToReturn = cellNameToReturn else {
            XCTFail("You did not set `cellNameToReturn`")
            return UUID().uuidString
        }
        
        return cellNameToReturn
    }
}
