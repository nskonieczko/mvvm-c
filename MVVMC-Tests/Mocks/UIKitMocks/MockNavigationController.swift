//
//  MockNavigationController.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

class MockNavigationController: UINavigationController {
    var viewControllerPushed: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        viewControllerPushed = viewController
    }
}
