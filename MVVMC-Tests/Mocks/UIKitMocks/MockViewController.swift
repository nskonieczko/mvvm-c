//
//  MockViewController.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

class MockViewController: UIViewController {
    var viewControllerPresented: UIViewController?
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
    }
}
