//
//  MockAPIClient.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation

@testable import MVVM_C

class MockAPIClient: APIClientProtocol {
    var getCustomerDataCompletions: [(Customer) -> Void] = []
    
    var getAPIDataCallCount: Int {
        getCustomerDataCompletions.count
    }
    
    func getCustomerData(customerID: String, _ completion: @escaping (Customer) -> Void) {
        getCustomerDataCompletions.append(completion)
    }
}
