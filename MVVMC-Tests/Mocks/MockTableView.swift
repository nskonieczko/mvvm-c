//
//  MockTableView.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import UIKit
import XCTest

class MockTableView: UITableView {
    var expectation: XCTestExpectation?
    var reloadDataCallCount = 0
    
    override func reloadData() {
        reloadDataCallCount += 1
        expectation?.fulfill()
    }
}
