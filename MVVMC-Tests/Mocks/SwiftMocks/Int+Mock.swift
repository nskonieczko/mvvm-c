//
//  Int+Mock.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation

extension Int {
    static var mock: Int {
        Int.random(in: 0...999)
    }
}
