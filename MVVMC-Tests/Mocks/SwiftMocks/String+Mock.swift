//
//  String+Mock.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation

extension String {
    static var mock: String {
        UUID().uuidString
    }
}
