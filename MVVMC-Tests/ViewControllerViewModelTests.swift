//
//  ViewControllerViewModelTests.swift
//  MVVM-CTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import Quick
import Nimble

@testable import MVVM_C

class ViewControllerViewModelTests: QuickSpec {
    override func spec() {
        describe("ViewControllerViewModel") {
            var viewModel: ViewControllerViewModel!
            var mockAPIClient: MockAPIClient!
            
            beforeEach {
                mockAPIClient = MockAPIClient()
                viewModel = ViewControllerViewModel(apiClient: mockAPIClient)
            }
            
            describe("route") {
                context("When indexPath row is zero") {
                    it("should return `.presentSecondScreen`") {
                        let indexPath = IndexPath(row: 0, section: 0)
                        let route = viewModel.route(for: indexPath)
                        expect(route).to(equal(.presentSecondScreen))
                    }
                }
                
                context("When indexPath row is one") {
                    it("should return `.pushSecondScreen`") {
                        let indexPath = IndexPath(row: 1, section: 0)
                        let route = viewModel.route(for: indexPath)
                        expect(route).to(equal(.pushSecondScreen))
                    }
                }

                context("When indexPath row is anything other than 0 or 1") {
                    it("should return `.none` or nil") {
                        let indexPath = IndexPath(row: 15, section: 0)
                        let route = viewModel.route(for: indexPath)
                        expect(route == .none).to(beTrue())
                    }
                }
            }
            
            describe("numberOfRowsInSection") {
                context("when any section is called") {
                    it("should return 2") {
                        expect(viewModel.numberOfRowsInSection(.mock)).to(equal(2))
                    }
                }
            }
        }
    }
}
