//
//  ViewControllerTests.swift
//  TestingExampleTests
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import Quick
import Nimble
import UIKit

@testable import MVVM_C

class ViewControllerTests: QuickSpec {
    override func spec() {
        describe("ViewController") {
            var viewModel: ViewControllerViewModel!
            var mockTableView: MockTableView!
            var mockAPIClient: MockAPIClient!
            var mockMainCoordinator: MockMainCoordinator!
            var viewController: ViewController!
            
            beforeEach {
                mockTableView = MockTableView()
                mockAPIClient = MockAPIClient()
                mockMainCoordinator = MockMainCoordinator()
                viewModel = ViewControllerViewModel(apiClient: mockAPIClient)
                viewController = ViewController(viewModel: viewModel, coordinator: mockMainCoordinator)
                viewController.tableView = mockTableView
                
                _ = viewController.view
            }
            
            describe("didSelectRowAt") {
                context("when called row 0 is called") {
                    beforeEach {
                        viewController.tableView(mockTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
                    }
                    
                    it("should call to navigate to SecondViewController") {
                        expect(mockMainCoordinator.routeSelected).to(equal(.presentSecondScreen))
                    }
                }
                
                context("when called row 1 is called") {
                    beforeEach {
                        viewController.tableView(mockTableView, didSelectRowAt: IndexPath(row: 1, section: 0))
                    }
                    
                    it("should call to navigate to SecondViewController") {
                        expect(mockMainCoordinator.routeSelected).to(equal(.pushSecondScreen))
                    }
                }
            }
        }
    }
    
}
