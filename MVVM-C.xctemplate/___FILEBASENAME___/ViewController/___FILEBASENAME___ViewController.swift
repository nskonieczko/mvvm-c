import UIKit

class ___FILEBASENAMEASIDENTIFIER___: UIViewController {
    private let viewModel: ___VARIABLE_productName___ViewModelProtocol

    init(viewModel: ___VARIABLE_productName___ViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
    }
}

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_productName___ViewModelDelegate {

}
