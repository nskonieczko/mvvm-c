import Foundation

protocol ___FILEBASENAMEASIDENTIFIER___Delegate: AnyObject {

}

protocol ___FILEBASENAMEASIDENTIFIER___Protocol: AnyObject {
    var delegate: ___VARIABLE_productName___ViewModelDelegate? { get set }
}

class ___FILEBASENAMEASIDENTIFIER___: ___FILEBASENAMEASIDENTIFIER___Protocol {
    weak var delegate: ___VARIABLE_productName___ViewModelDelegate?

    init() { }
}
