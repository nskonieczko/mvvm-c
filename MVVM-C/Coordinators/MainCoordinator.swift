//
//  MainCoordinator.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    enum Route {
        case presentSecondScreen
        case pushSecondScreen
        case none
    }
    
    func navigate(_ context: UIViewController, to route: Route) {
        switch route {
        case .pushSecondScreen:
            let apiClient = CurrencyAPIClient()
            let viewModel = SecondViewModel(apiClient: apiClient)
            let destinationViewController = SecondViewController(viewModel: viewModel)
            context.navigationController?.pushViewController(destinationViewController, animated: true)
            
        case .presentSecondScreen:
            let apiClient = CurrencyAPIClient()
            let viewModel = SecondViewModel(apiClient: apiClient)
            let destinationViewController = SecondViewController(viewModel: viewModel)
            context.present(destinationViewController, animated: true)
            
        case .none:
            break
        }
    }
}
