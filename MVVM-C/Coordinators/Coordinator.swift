//
//  Coordinator.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

protocol Coordinator {
    associatedtype Route
    func navigate(_ context: UIViewController, to route: Route)
}
