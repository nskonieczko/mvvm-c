//
//  SecondViewController.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/15/22.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    private let viewModel: SecondViewModelProtocol
    
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    init(viewModel: SecondViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
        
        setup()
        
        viewModel.getCurrencies()
    }
    
    private func setup() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        tableView.delegate = self
        tableView.dataSource = self
        viewModel.delegate = self

        tableView.register(MyTableViewCell.self, forCellReuseIdentifier: MyTableViewCell.identifier)
    }
}

extension SecondViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier) as? MyTableViewCell else {
            return UITableViewCell()
        }

        let name = viewModel.cellName(for: indexPath)
        cell.configure(with: name)

        return cell
    }
}

extension SecondViewController: UITableViewDelegate { }

extension SecondViewController: SecondViewModelDelegate {
    func secondViewModel(_ viewModel: SecondViewModelProtocol, didGet currencies: [Currency]) {
        tableView.reloadData()
    }
}
