//
//  SecondViewModel.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 7/11/22.
//

import Foundation

protocol SecondViewModelDelegate: AnyObject {
    func secondViewModel(_ viewModel: SecondViewModelProtocol, didGet currencies: [Currency])
}

protocol SecondViewModelProtocol: AnyObject {
    var delegate: SecondViewModelDelegate? { get set }
    
    func getCurrencies()
    func numberOfRowsInSection(_ section: Int) -> Int
    func cellName(for indexPath: IndexPath) -> String
}

class SecondViewModel: SecondViewModelProtocol {
    private let apiClient: CurrencyAPIClientProtocol
    private var currencies: [Currency] = []
    
    weak var delegate: SecondViewModelDelegate?
    
    init(apiClient: CurrencyAPIClientProtocol) {
        self.apiClient = apiClient
    }
    
    func getCurrencies() {
        apiClient.getCurrencies { [weak self] currencies in
            guard let self else {
                return
            }
            
            self.currencies = currencies
            self.delegate?.secondViewModel(self, didGet: currencies)
        }
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return currencies.count
    }
    
    func cellName(for indexPath: IndexPath) -> String {
        return currencies[indexPath.row].name
    }
}
