//
//  Currency.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 7/11/22.
//

import Foundation

struct Currency: Codable {
    let id: String
    let name: String
    let minSize: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case minSize = "min_size"
    }
}
