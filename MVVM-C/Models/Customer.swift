//
//  Customer.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 6/16/22.
//

import Foundation

struct Customer: Decodable {
    let firstName: String
    let lastName: String
}
