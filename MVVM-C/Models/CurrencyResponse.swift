//
//  CurrencyResponse.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 7/11/22.
//

import Foundation

struct CurrencyResponse: Codable {
    let data: [Currency]
}
