//
//  APIClient.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation

protocol APIClientProtocol {
    func getCustomerData(customerID: String, _ completion: @escaping (Customer) -> Void)
}

// This is just to "mock" an API call
class APIClient: APIClientProtocol {
    func getCustomerData(customerID: String, _ completion: @escaping (Customer) -> Void) {
        // This is just some mocked/dummy example APIClient layer. Here is where you would create your specific endpoint code.
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            completion(.init(firstName: "Person", lastName: "Doe"))
        }
        
        // AFNetworking code here.
    }
}
