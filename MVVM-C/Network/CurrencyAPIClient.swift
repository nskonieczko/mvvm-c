//
//  CurrencyAPIClient.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 7/11/22.
//

import Foundation
import Moya

/*
 Example of Moya API Client Layer
 */

protocol CurrencyAPIClientProtocol {
    func getCurrencies(_ completion: @escaping ([Currency]) -> Void)
}

class CurrencyAPIClient: CurrencyAPIClientProtocol {
    private let provider = MoyaProvider<CurrencyRouter>()
    private let jsonDecoder = JSONDecoder()
    
    func getCurrencies(_ completion: @escaping ([Currency]) -> Void) {
        provider.request(.getCurrency) { result in
            switch result {
            case let .success(response):
                do {
                    let currencies = try self.jsonDecoder.decode(CurrencyResponse.self, from: response.data)
                    completion(currencies.data)
                } catch {
                    completion([])
                }
            case .failure:
                completion([])
            }
        }
    }
}
