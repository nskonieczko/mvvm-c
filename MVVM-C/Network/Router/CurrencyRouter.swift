//
//  CurrencyRouter.swift
//  MVVM-C
//
//  Created by Nick Konieczko on 7/11/22.
//

import Foundation
import Moya

enum CurrencyRouter {
    case getCurrency
}

extension CurrencyRouter: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.coinbase.com/v2") else {
            return URL(fileURLWithPath: "")
        }
        
        return url
    }
    
    var path: String {
        switch self {
        case .getCurrency:
            return "/currencies"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getCurrency:
            return .get
        }
    }
    
    var task: Moya.Task {
        switch self {
        case .getCurrency:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getCurrency:
            return ["Content-Type": "application/json"]
        }
    }
    
}
