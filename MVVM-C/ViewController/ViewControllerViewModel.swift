//
//  ViewControllerViewModel.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/8/22.
//

import Foundation

protocol ViewControllerViewModelProtocol: AnyObject {
    var delegate: ViewControllerViewModelDelegate? { get set }

    func numberOfRowsInSection(_ section: Int) -> Int
    func getAPIData()
    func route(for indexPath: IndexPath) -> MainCoordinator.Route
    func cellName(for indexPath: IndexPath) -> String
}

protocol ViewControllerViewModelDelegate: AnyObject {
    func viewControllerViewModel(_ viewModel: ViewControllerViewModel)
}

class ViewControllerViewModel: ViewControllerViewModelProtocol {
    private let apiClient: APIClientProtocol
    private var customer: Customer?
    
    weak var delegate: ViewControllerViewModelDelegate?
    
    init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return 2
    }
    
    func getAPIData() {
        apiClient.getCustomerData(customerID: "123") { [weak self] customer in
            guard let self = self else {
                return
            }
            
            self.customer = customer
            self.delegate?.viewControllerViewModel(self)
        }
    }
    
    func route(for indexPath: IndexPath) -> MainCoordinator.Route {
        switch indexPath.row {
        case 0:
            return .presentSecondScreen
        case 1:
            return .pushSecondScreen
            
        default:
            return .none
        }
    }

    func cellName(for indexPath: IndexPath) -> String {
        switch indexPath.row {
        case 0:
            return "Present SecondViewController"
        case 1:
            return "Push SecondViewController"
        default:
            return "N/A"
        }
    }
}

