//
// Created by Nick Konieczko on 7/6/22.
//

import UIKit

class MyTableViewCell: UITableViewCell {
    func configure(with name: String) {
        textLabel?.text = name
    }
}
