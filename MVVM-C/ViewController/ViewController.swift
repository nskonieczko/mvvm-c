//
//  ViewController.swift
//  TestingExample
//
//  Created by Nick Konieczko on 6/8/22.
//

import UIKit

class ViewController: UIViewController {
    private let viewModel: ViewControllerViewModelProtocol
    private let coordinator: MainCoordinator
    
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    // Cannot inject using Coordinator protocol due to associatedtype Route
    init(viewModel: ViewControllerViewModelProtocol, coordinator: MainCoordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.getAPIData()
    }
    
    private func setup() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        tableView.delegate = self
        tableView.dataSource = self
        viewModel.delegate = self

        tableView.register(MyTableViewCell.self, forCellReuseIdentifier: MyTableViewCell.identifier)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier) as? MyTableViewCell else {
            return UITableViewCell()
        }

        let name = viewModel.cellName(for: indexPath)
        cell.configure(with: name)

        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let route = viewModel.route(for: indexPath)
        coordinator.navigate(self, to: route)
    }
}

extension ViewController: ViewControllerViewModelDelegate {
    func viewControllerViewModel(_ viewModel: ViewControllerViewModel) {
        tableView.reloadData()
    }
}

