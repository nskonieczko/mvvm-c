//
// Created by Nick Konieczko on 7/6/22.
//

import UIKit

extension UITableViewCell {
    static var identifier: String {
        "\(Self.self)"
    }
}
